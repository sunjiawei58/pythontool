import socket

def Convert2Bytes(val, bytesArray):
    systemLength = 32
    testArray = bin(val)[2:].zfill(32)
    bytesArray.append(int(testArray[systemLength - 8:systemLength], 2))
    bytesArray.append(int(testArray[systemLength - 16:systemLength - 8], 2))
    bytesArray.append(int(testArray[systemLength - 24:systemLength - 16], 2))
    bytesArray.append(int(testArray[0:systemLength - 24], 2))

def constructMSG(bytes)
class CMessage :
    def __init__(self):
        self.system = 16909320
        self.command = 0
        self.source = 32
        self.target = 0
        self.length = 0
        self.iParam1 = 0
        self.iParam2 = 0
        self.iParam3 = 0
        self.dataBuf = bytearray()




HOST = '192.168.56.101'

PORT = 39000  #test for rtb server


msg = CMessage()
cmdStr = "This is a test String "
msg.length = len(cmdStr)
msg.dataBuf = bytearray(cmdStr,'utf-8')


testArray = bin(msg.system)[2:].zfill(32)

systemLength = len(testArray)
bytes = bytearray()

Convert2Bytes(msg.system,bytes)
Convert2Bytes(msg.command,bytes)
Convert2Bytes(msg.source,bytes)
Convert2Bytes(msg.target,bytes)
Convert2Bytes(msg.iParam1,bytes)
Convert2Bytes(msg.iParam2,bytes)
Convert2Bytes(msg.iParam3,bytes)
bytes += msg.dataBuf



with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as s:
    s.connect((HOST,PORT))
    s.sendall(bytes)
    data = s.recv(1024)

print('Received',repr(data))
